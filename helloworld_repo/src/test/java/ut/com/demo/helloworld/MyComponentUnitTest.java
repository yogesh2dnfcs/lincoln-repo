package ut.com.demo.helloworld;

import org.junit.Test;
import com.demo.helloworld.api.MyPluginComponent;
import com.demo.helloworld.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}