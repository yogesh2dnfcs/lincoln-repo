package com.demo.helloworld.api;

public interface MyPluginComponent
{
    String getName();
}